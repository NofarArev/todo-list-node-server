var express = require("express");
var app = express();
var cors = require('cors')
const fs = require('fs');

app.use(cors());

app.get('/getData', function (req, res) {
    console.log("getDataReceived");
    fs.readFile('./data/data.json', (err, content) => {
        if (err) {
            console.log("error read: " + err);
            res.writeHead(400, "cannot read from file");
        } else {
            console.log(content);
            res.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
            res.write(content);
        }
        res.end();
    });


});

app.post('/saveData', function (req, res) {
    console.log("saveDataReceived");
    fs.writeFile('./data/data.json', req.query[0], (err) => {
        console.log(req.query[0]);
        if (err) {
            console.log("error write to file " + err);
            res.writeHead(400, "cannot read from file");
        } else {
            res.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
            res.write(JSON.stringify(require('./data/data.json')));
        }

        res.end();
    });

});

app.listen(3001, function () {
    console.log("Started on PORT 3001");
});